---
## Studying Mumbai's Dabbawala food-delivery system using ClearBlade AssetMonitor


---

## Introduction

The Dabbawala food delivery service in Mumbai, India (https://en.wikipedia.org/wiki/Dabbawala) allows daily delivery of home-cooked meals from an individual’s home to their place of work such that the meal arrives at lunch time, hot and fresh, in spite of the commute from the individual’s home to their workplace being as long as 2 hrs.  The service is word renowned for its efficiency and accuracy in spite of being quite low-tech. 

According to the Wikipedia article referenced, to-date there has not been a rigorous study of the service done to demonstrate its true accuracy and efficiency.  Evidence of the service’s accuracy and efficiency is anecdotal.


---

## Dabbawala Asset Monitor

The described system uses ClearBlade’s Asset Monitor to monitor 200 dabbas or lunchboxes (0.1% of the 200,000 delivered daily) over an extended period of time to capture the following information:

1. Average delivery rates (delivery distance / delivery time).
2. Correlation between delivery rates
	a. routes
	b. modes of transport (main modes of transport used are trains and bicycles) 
3. Delivery errors: whenever a dabba is lost or delivered to the wrong recipient.

200 volunteer families will be chosen from all residential areas of Mumbai. The main criterion for choice will be that the itinerary of the dabba will be significant (about 2 hrs). 

The volunteer families will use special dabbas equipped with Nimblelink cellular modules. 

---